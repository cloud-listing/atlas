from flask import Flask
from flask_restful import Api, Resource
import requests
from flask_cors import CORS


app = Flask(__name__)
api = Api(app)
cors = CORS(app, resources={r"/*": {"origins": "*"}})


AIVEN_URL = 'https://api.aiven.io'


CLOUD_SERVICES_MAPPING = {
    "Amazon Web Services" : "Amazon Web Services",
    "Google Cloud" : "Google Cloud Platform",
    "Azure" : "Microsoft Azure",
    "DigitalOcean" : "DigitalOcean",
    "UpCloud" : "UpCloud",
}



class GetCloud(Resource):
    def get(self):

        output_data = {}
        
        try:
            response = requests.get(AIVEN_URL + "/v1/clouds")
            if response.status_code == 200:
                response_json = response.json()

                clouds = []
                for row in response_json['clouds']:
                    cloud_provider = [value for key, value in CLOUD_SERVICES_MAPPING.items() if key.lower() in row['cloud_description'].lower() ]
                    if not cloud_provider:
                        print(row)
                        
                    clouds.append({'provider' : cloud_provider[0], 'cloud_name' : row['cloud_name'], "latitude" : row['geo_latitude'], "longitude" : row["geo_longitude"], "region" : row["geo_region"]})

            output_data = {'data' : clouds, 'status' : 'success'}
        except Exception as e:
            output_data = {'status' : 'failed'}

        return output_data



api.add_resource(GetCloud, "/clouds")


if __name__ == "__main__":
    app.run(host='0.0.0.0')