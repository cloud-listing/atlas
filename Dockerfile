FROM python:3.7-alpine3.12

RUN apk update

WORKDIR /app

COPY . ./

RUN pip install -r requirements.txt

ENTRYPOINT ["python3", "/app/server.py"]